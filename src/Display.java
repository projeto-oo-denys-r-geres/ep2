import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Display extends JPanel {
	
	Snake snake = new Snake();
	private Wall wall = new Wall();
	private Fruit fruit = new Fruit();
	private BigFruit bigfruit= new BigFruit();
	private DecreaseFruit decreasefruit = new DecreaseFruit();
	private BombFruit bombfruit = new BombFruit();
	private int contador =0;
	
	public void paint(Graphics g) {
		super.paint(g);
		setBackground(Color.BLACK);
		ImageIcon head = new ImageIcon("imagens/head.png");
		ImageIcon body = new ImageIcon("imagens/body.png");
		ImageIcon wall1= new ImageIcon("imagens/muro.png");
		ImageIcon fruit1= new ImageIcon("imagens/frutavermelha.png");
		ImageIcon fruitbig= new ImageIcon("imagens/frutamarela.png");
		ImageIcon decreasefruit1 = new ImageIcon("imagens/frutaazul.png");
		ImageIcon bombfruit1 = new ImageIcon("imagens/bomba.png");
		
		final Image cabeca; 
		final Image muro;
		final Image corpo;
		final Image fruta;
		final Image frutagrande;
		final Image frutadecresce;
		final Image frutabomba;
		
		cabeca = head.getImage();
		muro=wall1.getImage();
		corpo = body.getImage();
		fruta= fruit1.getImage();
		frutagrande= fruitbig.getImage();
		frutadecresce=decreasefruit1.getImage();
		frutabomba =bombfruit1.getImage();
	
		//PLOTA A CABEÇA E O CORPO DA COBRA
		if(snake.isLife()) {
		for(int i =0; i<snake.getSize();i++) {
			if(i==0)
			g.drawImage(cabeca,snake.getX(0), snake.getY(0), this); 
			else
				g.drawImage(corpo,snake.getX(i), snake.getY(i), this);
		}
		
		
		for(int j=0;j<25;j++) {
			g.drawImage(muro,wall.getX_Wall(j),wall.getY_Wall(j),this);
		}
		
		contador++;
		//PLOTA AS FRUTAS
		if(contador>0&&contador<170) 
		g.drawImage(fruta,fruit.getX_fruit(),fruit.getY_fruit(),this);
		
		if(contador>100&&contador<250) 
		g.drawImage(frutagrande,bigfruit.getX_fruit(),bigfruit.getY_fruit(),this);
		
		if(contador>200&&contador<270) 
		g.drawImage(frutadecresce,decreasefruit.getX_fruit(), decreasefruit.getY_fruit(),this);
		
		if(contador>260&&contador<290) 
		g.drawImage(frutabomba, bombfruit.getX_fruit(),bombfruit.getY_fruit(),this);
		
		if(contador>290)
			contador=0;
	   
		//PLOTA O SCORE
		String score= "Score: "+(fruit.getScore()+bigfruit.score+decreasefruit.getScore());
		Font estilo = new Font("TimesRoman",Font.ITALIC,15);
		g.setFont(estilo);
		g.setColor(Color.WHITE);
		g.drawString(score, 160, 15);
		
		}
		//PLOTA MENSAGEM NA TELA DE GAME OVER
		if(!snake.isLife()){
			String score= "Score Final: "+(fruit.getScore()+bigfruit.score+decreasefruit.getScore());
			Font estilo = new Font("Verdana",Font.BOLD,20);
			g.setFont(estilo);
			g.setColor(Color.RED);
			g.drawString(score, 120, 195);
			
			String gameover= "GAME OVER";
			Font style = new Font("TimesRoman",Font.BOLD,30);
			g.setFont(style);
			g.setColor(Color.white);
			g.drawString(gameover, 80, 160);	
		}
	
	}
	
	public void setSnake(Snake snake) {
		this.snake=snake;
	}
	
	public Snake getSnake() {
		return snake;
	}
	
	public void setFruit(Fruit fruit) {
		this.fruit=fruit;
	}
	
	public Fruit getFruit() {
		return fruit;
	}

	public Wall getWall() {
		return wall;
	}

	public void setWall(Wall wall) {
		this.wall = wall;
	}

	public BigFruit getBigfruit() {
		return bigfruit;
	}

	public void setBigfruit(BigFruit bigfruit) {
		this.bigfruit = bigfruit;
	}

	public DecreaseFruit getDecreasefruit() {
		return decreasefruit;
	}

	public void setDecreasefruit(DecreaseFruit decreasefruit) {
		this.decreasefruit = decreasefruit;
	}

	public BombFruit getBombfruit() {
		return bombfruit;
	}

	public void setBombfruit(BombFruit bombfruit) {
		this.bombfruit = bombfruit;
	}

}
