import java.util.Random;

public class Fruit {
	protected int x_fruit;
	protected int y_fruit;
	protected int score;
	
	
	public Fruit() {
		
		Random random = new Random();
	
		x_fruit = random.nextInt(40)*10;
		y_fruit=random.nextInt(35)*10;
	}
	
	public void GerarPosicao(){
		Random random = new Random();
		x_fruit = random.nextInt(40)*10;
		y_fruit=random.nextInt(35)*10;
		
	}

	public int getX_fruit() {
		return x_fruit;
	}

	public void setX_fruit(int x_fruit) {
		this.x_fruit = x_fruit;
	}

	public int getY_fruit() {
		return y_fruit;
	}

	public void setY_fruit(int y_fruit) {
		this.y_fruit = y_fruit;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	

}
