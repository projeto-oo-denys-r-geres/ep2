
public class MinhaThread extends Thread {
	protected Snake snake;
	protected Display display;
	protected Movements movimentos;
	MinhaThread(Display display){
		this.display=display;
	}
	public void run() {
		while(display.getSnake().isLife()) {
		try {
			Thread.sleep(95);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(display.getSnake().isRight()||display.getSnake().isLeft()||display.getSnake().isDown()||display.getSnake().isUp())
			display.getSnake().move();
		//COMER FRUITA
		movimentos = new Movements(display.getSnake(),display.getWall(),display.getFruit());
		movimentos.ComerFruta();
		//COLISOES
		movimentos.Colisoes();
		movimentos.ColisoesBarrera();
		//BIG FRUIT
		movimentos = new Movements(display.getSnake(),display.getWall(),display.getBigfruit());
		movimentos.ComerBigFruta();
		//DECREASE FRUIT
		movimentos = new Movements(display.getSnake(),display.getWall(),display.getDecreasefruit());
		movimentos.ComerDrecreaseFruit();
		//BOMB FRUIT
		movimentos = new Movements(display.getSnake(),display.getWall(),display.getBombfruit());
		movimentos.ComerBombFruit();
		
		display.repaint();

	}

}
	}
