
public class Movements {
	private Snake snake;
	private Wall wall;
	private Fruit fruit;
	
	public Movements(Snake snake, Wall wall,Fruit fruit) {
		this.snake=snake;
		this.wall=wall;
		this.fruit=fruit;
		
	}
	
	public void ComerFruta() {
		if(snake.getX(0)==fruit.getX_fruit() && snake.getY(0)==fruit.getY_fruit()) {
			snake.setSize(snake.getSize()+1);
			fruit.setScore(fruit.getScore()+1);
			fruit.GerarPosicao();	
		}
	}
	
	public void ComerBigFruta() {
		if(snake.getX(0)==fruit.getX_fruit() && snake.getY(0)==fruit.getY_fruit()) {
			snake.setSize(snake.getSize()+1);
			fruit.setScore(fruit.getScore()+2);
			fruit.GerarPosicao();	
		}	
	}
	
	public void ComerDrecreaseFruit() {
		if(snake.getX(0)==fruit.getX_fruit() && snake.getY(0)==fruit.getY_fruit()) {
			snake.setSize(3);
			fruit.setScore(fruit.getScore()+1);
			fruit.GerarPosicao();
		}
	}
	
	public void ComerBombFruit() {
		if(snake.getX(0)==fruit.getX_fruit()&& snake.getY(0)==fruit.getY_fruit()) {
			snake.setLife(false);
		}
	}
	public void Colisoes() {
		for(int i=1; i<snake.getSize();i++) {
		if(snake.getX(0)==snake.getX(i)&&snake.getY(0)==snake.getY(i)) {
		snake.setLife(false);
		}	
      }	
	}
	public void ColisoesBarrera() {
		for(int i=0;i<25;i++) {
			if(snake.getX(0)==wall.getX_Wall(i)&&snake.getY(0)==wall.getY_Wall(i)) {
			snake.setLife(false);
			}
		}
	}
}

