import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

class Principal extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Display display = new Display();
	private MinhaThread ThreadID= new MinhaThread(display);
	
	public static void main (String[] args) {
		new Principal(); 
}
	public Principal() {
		add(display);
		ThreadID.start();
		setSize(400,400);
		setTitle("Snake Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setBackground(Color.black);
		setLocationRelativeTo(null); 
		setVisible(true);
		setResizable(false);
		
		
		addKeyListener(new KeyListener(){
			@Override

			public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {
			int k=e.getKeyCode();
			if(k==KeyEvent.VK_RIGHT) {
				display.getSnake().setRight(true);
				display.getSnake().setLeft(false);
				display.getSnake().setUp(false);
				display.getSnake().setDown(false);
				
			}
			else if(k==KeyEvent.VK_LEFT) {
				display.getSnake().setLeft(true);
				display.getSnake().setRight(false);
				display.getSnake().setUp(false);
				display.getSnake().setDown(false);
			}
			else if(k==KeyEvent.VK_UP) {
				display.getSnake().setLeft(false);
				display.getSnake().setRight(false);
				display.getSnake().setUp(true);
				display.getSnake().setDown(false);			
			}
			else if(k==KeyEvent.VK_DOWN) {
				display.getSnake().setDown(true);
				display.getSnake().setRight(false);
				display.getSnake().setLeft(false);
				display.getSnake().setUp(false);
			}
	
		}

		@Override
		public void keyReleased(KeyEvent e) {

		}

		});
	}
}
