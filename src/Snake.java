

public class Snake{
	
		private int size;
		private int [] x = new int [400];
		private int [] y = new int [400];
		private boolean life=true;
		private boolean right;
		private boolean left;
		private boolean up;
		private boolean down;
		
		
		public Snake() {
			size=3;
			
			x[0] = 90;
			x[1] = 80;		
			x[2] = 70;
			
			y[0] = 100;
			y[1] = 100;
			y[2] = 100;

	}
		public void move() {
			for(int i=size-1;i>0;i--) { 
				x[i]=x[i-1];
				y[i]=y[i-1];
			}
			if(up) {
				y[0]-=10;
				if(y[0]<0) {
					y[0]=350;
					//life=false;
					
				}	
				
			}
			else if(down) {
				y[0]+=10; 
				if(y[0]>350) {
					y[0]=0;
					//life=false;
				}
				
				
			}
			else if(right) {
				x[0]+=10;
				 if(x[0]>400) {
					x[0]=0;
					// life=false;
				}
				
			}
			else if(left) {
				x[0]-=10;
				if(x[0]<0) {
					x[0]=400;
					//life=false;
				}
				
			}
			

		}
		
		
		
		
		public int getSize() {
			return size;
		}
		public void setSize(int size) {
			this.size = size;
		}
		public int[] getX() {
			return x;
		}
		public void setX(int[] x) {
			this.x = x;
		}
		public int[] getY() {
			return y;
		}
		public void setY(int[] y) {
			this.y = y;
		}
	
		public boolean isRight() {
			return right;
		}
		public void setRight(boolean right) {
			this.right = right;
		}
		public boolean isLeft() {
			return left;
		}
		public void setLeft(boolean left) {
			this.left = left;
		}
		public boolean isUp() {
			return up;
		}
		public void setUp(boolean up) {
			this.up = up;
		}
		public boolean isDown() {
			return down;
		}
		public void setDown(boolean down) {
			this.down = down;
		}
		public int getY(int i) {
			return y[i];
		}
		public int getX(int i) {
			return x[i];
		}
		public boolean isLife() {
			return life;
		}
		public void setLife(boolean life) {
			this.life = life;
		}
		
	}



