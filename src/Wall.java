
public class Wall {
	private int[] X_Wall = new int [30];
	private int[] Y_Wall = new int [30];
	
	public Wall() {
		X_Wall[0]=200;
		X_Wall[1]=210;
		X_Wall[2]=220;
		X_Wall[3]=230;
		X_Wall[4]=240;
		X_Wall[5]=250;
		X_Wall[6]=260;
		X_Wall[7]=270;
		X_Wall[8]=280;
		X_Wall[9]=290;
		
		X_Wall[10]=350;
		X_Wall[11]=350;
		X_Wall[12]=350;
		X_Wall[13]=350;
		X_Wall[14]=350;
		
		X_Wall[15]=360;
		X_Wall[16]=360;
		X_Wall[17]=360;
		X_Wall[18]=360;
		X_Wall[19]=360;
		
		X_Wall[20]=20;
		X_Wall[21]=30;
		X_Wall[22]=40;
		X_Wall[23]=120;
		X_Wall[24]=130;
		
		Y_Wall[0]=130;
		Y_Wall[1]=130;
		Y_Wall[2]=130;
		Y_Wall[3]=130;
		Y_Wall[4]=130;
		Y_Wall[5]=130;
		Y_Wall[6]=130;
		Y_Wall[7]=130;
		Y_Wall[8]=130;
		Y_Wall[9]=130;
		
		Y_Wall[10]=210;
		Y_Wall[11]=220;
		Y_Wall[12]=230;
		Y_Wall[13]=240;
		Y_Wall[14]=250;
		
		Y_Wall[15]=260;
		Y_Wall[16]=270;
		Y_Wall[17]=280;
		Y_Wall[18]=290;
		Y_Wall[19]=300;
		
		Y_Wall[20]=250;
		Y_Wall[21]=260;
		Y_Wall[22]=270;
		Y_Wall[23]=300;
		Y_Wall[24]=300;
	}

	public int[] getX_Wall() {
		return X_Wall;
	}

	public void setX_Wall(int[] x_Wall) {
		this.X_Wall = x_Wall;
	}

	public int[] getY_Wall() {
		return Y_Wall;
	}

	public void setY_Wall(int[] y_Wall) {
		this.Y_Wall = y_Wall;
	}
	public int getY_Wall(int i) {
		return Y_Wall[i];
	}
	public int getX_Wall(int i) {
		return X_Wall[i];
	}
			

}
